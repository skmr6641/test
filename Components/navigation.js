
import * as React from 'react';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import Home from '../Screens/Home';
import Profile from '../Screens/Profile';
import Detail from '../Screens/Detail';
import Image from '../Screens/Image';
import HomeScreenTab from '../Screens/Home';
import TabNavigator from '../Components/route'




const Stack = createNativeStackNavigator();




export default function Nav() {
    console.log("Mintoo")
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false,
                    gestureEnabled: true,
                    gestureDirection: 'horizontal',
                }}>
                <Stack.Screen name='Home' component={HomeScreenTab} />
                <Stack.Screen name='TabNavigator' component={TabNavigator} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}