import React, { Component } from "react";
import { View, Text, Image, ScrollView, TextInput, StyleSheet } from 'react-native';
import { Checkbox, RadioButton } from 'react-native-paper';
//import RadioGroup from 'react-native-radio-buttons-group';




export default class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            value: 'first',
            value2: 'second'

        }

    }



    render() {

        return (



            <View style={{ flex: 1, backgroundColor: 'white' }}>


                <ScrollView>

                    <View style={{ height: 70, width: '100%', flexDirection: 'row' }}>

                        <View style={{ height: 70, width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                            <Image
                                source={require('../Assets/drawer.png')}
                                style={{ height: 30, width: 30 }}
                            />
                        </View>
                        <View style={{ height: 70, width: '60%', alignItems: 'flex-end', justifyContent: 'center' }}>
                            <Image
                                source={require('../Assets/india.png')}
                                style={{ height: 30, width: 30 }}
                            />
                        </View>
                        <View style={{ height: 70, width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ height: 30, width: 30 }}
                                source={require('../Assets/admin.png')}
                            />
                        </View>

                    </View>
                    {/*------------- End of header ---------------*/}

                    <View style={{ height: 100, width: '90%', alignSelf: 'center', margin: 10, justifyContent: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 30, color: 'black' }}>New Opportunity</Text>

                    </View>
                    {/*------------ End of oppotunity -----------*/}


                    <View style={{ height: 60, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: 'black' }}>Opportunity Source:*</Text>
                        <TextInput style={{ height: 40, width: '100%', borderWidth: 1 }} />
                    </View>

                    <View style={{ height: 60, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: 'black' }}>Source Detail</Text>
                        <TextInput style={{ height: 40, width: '100%', borderWidth: 1 }} />
                    </View>

                    {/*-----------------start main view1-------- */}
                    <View style={{ height: 60, width: '90%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ height: 60, width: '40%' }}>
                            <Text style={{ color: 'black' }}>Assign To:*</Text>
                            <TextInput style={{ height: 40, width: '100%', borderWidth: 1 }} />
                        </View>
                        <View style={{ height: 60, width: '40%' }}>
                            <Text style={{ color: 'black' }}>Curency:*</Text>
                            <TextInput style={{ height: 40, width: '100%', borderWidth: 1 }} />
                        </View>
                    </View>
                    {/*-----------------end main view1-------- */}
                    <View style={{ height: 60, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: 'black' }}>Price List:*</Text>
                        <TextInput style={{ height: 40, width: '40%', borderWidth: 1 }} />
                    </View>
                    {/* ------End price List---------------- */}

                    <View style={{ height: 50, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: '#727cf5', fontWeight: 'bold', fontSize: 15, borderBottomWidth: 1 }}>PROJECT DETAIL</Text>

                    </View>

                    {/*----- Start project detail ----- */}
                    <View style={{ height: 60, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: 'black' }}>Project Name:*</Text>
                        <TextInput style={{ height: 40, width: '100%', borderWidth: 1 }}
                            placeholder='Enter Project Name ' />
                    </View>
                    {/* ------End project detail ------- */}


                    {/*----- Start concern department ----- */}
                    <View style={{ height: 60, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: 'black' }}>Concern Department:*</Text>
                        <TextInput style={{ height: 40, width: '100%', borderWidth: 1, color: 'black' }}
                            placeholder='Enter Project Name '
                            defaultValue='Unknown' />
                    </View>
                    {/* ------End concern department------- */}


                    <View style={{ height: 60, width: '90%', alignSelf: 'center', flexDirection: 'row' }}>

                        <Checkbox

                            status={this.state.checked ? 'checked' : 'unchecked'}
                            onPress={() => {
                                // setChecked(!checked);
                                this.setState({ checked: true })
                            }}
                        />
                        <Text style={{ margin: 8, color: 'black' }}>Has Physical Address:</Text>
                    </View>


                    <View style={{ height: 30, width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: '#727cf5', fontWeight: 'bold', fontSize: 15, borderBottomWidth: 1 }}>CUSTOMER</Text>

                    </View>

                    {/* ------------Start radio Button Customer  also ------------------ */}

                    <View style={{ height: 110, width: '90%', alignSelf: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <RadioButton

                            value="first"
                            status={this.state.checked1 === 'first' ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({ checked1: 'first' })}

                        />
                        <Text style={{color:'black',margin:7}}>Old Customer</Text>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                        <RadioButton

                            value="second"
                            status={this.state.checked1 === 'second' ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({ checked1: 'second' })}
                        />
                         <Text style={{color:'black',margin:7}}>New Customer</Text>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="Third"
                            status={this.state.checked1 === 'third' ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({ checked1: 'third' })}
                            s
                        />
                        <Text style={{color:'black',margin:7}}>Undefine</Text>
                        </View>
                    </View>
                    {/* ------------End radio Button Customer--------- */}




                    {/* --------------Start Button Architech   also------------------------------------- */}

                    <View style={{ height:120, width: '90%', alignSelf: 'center', }}>
                        <Text style={{ color: '#727cf5', fontWeight: 'bold', fontSize: 15, borderBottomWidth: 1 }}>ARACHITECH/CONSALTANT</Text>
                        <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value2="first"
                            status={this.state.checked2 === 'first' ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({ checked2: 'first' })}

                        />
                         <Text style={{ color: 'black',margin:7 }}>Old Architecj/Consultant</Text>
                        </View>
                       <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value2="second"
                            status={this.state.checked2 === 'second' ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({ checked2: 'second' })}
                        />
                         <Text style={{ color: 'black',margin:7 }}>Old Architecj/Consultant</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <RadioButton
                                value2="Third"
                                status={this.state.checked2 === 'third' ? 'checked' : 'unchecked'}
                                onPress={() => this.setState({ checked2: 'third' })}

                            />
                            <Text style={{ color: 'black' ,margin:7}}>Undefined</Text>
                        </View>
                    </View>

                 {/* --------------Start Button Architech   also------------------------------------- */}


                </ScrollView>
            </View>

        )
    }


}




