import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default  class ImageScreenTab extends Component {
  render() {
    return (
      <View style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor:'grey'
        }}>
        <Text>Image Page!</Text>
      </View>
    );
  }
}

