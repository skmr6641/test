import React, { Component } from 'react';
import { Text, View,TouchableOpacity } from 'react-native';

export default  class Detail extends Component {
  render() {
    return (
      <View style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor:'blue'
        }}>
        <Text>Detail  Page Hii!</Text>
        <TouchableOpacity 
        onPress={()=>{this.props.navigation.navigate('Form')}}
        style={{height:40, width:100, backgroundColor:'green'}}>

        </TouchableOpacity>
      </View>
    );
  }
}