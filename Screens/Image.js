import React, { Component } from 'react';
import { Text, View,TouchableOpacity } from 'react-native';

export default  class Image extends Component {
  render() {
    return (
      <View style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor:'red'
        }}>
        <Text>Image  Page Hii!</Text>
        <TouchableOpacity 
        onPress={()=>{this.props.navigation.navigate('Detail')}}
        style={{height:40, width:100, backgroundColor:'green'}}>

        </TouchableOpacity>
      </View>
    );
  }
}